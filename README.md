# sprocket
A spring boot HTTP API

## usage

Once the API is running, you can use it for the following mathematical operations:

### sum

Point your browser at:
http://localhost:8080/math/sum?number_one=0&number_two=6

### product

Point your browser at:

http://localhost:8080/math/product?number_one=0&number_two=6
